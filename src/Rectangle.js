//2D 4 sided geometrical figure with each angle 90 degrees

 export default class Rectangle {

  constructor(length, breadth) {

    this.length = length;
    this.breadth = breadth;

  }
   area() {
    var area = (length, breadth = this.length) => length * breadth;

    return (area(this.length, this.breadth));
  }

   perimeter() {
    var perimeter = (length, breadth = this.length) => 2 * (length + breadth);

    return (perimeter(this.length, this.breadth));
  }

}
module.exports = Rectangle;




