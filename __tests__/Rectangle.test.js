import  Rectangle from '../src/Rectangle';
describe('Rectangle', function () {
  describe('area', function () {
    it('should be 4 for length 1 and breadth 4', function () {
      let rectangle = new Rectangle(1, 4);
      let result = rectangle.area(1, 4);
      expect(result).toBe(4);
    });
    it('should be 16 for length 2 and breadth 8', function () {
      let rectangle = new Rectangle(2, 8);
      let result = rectangle.area();

      expect(result).toBe(16);
    });
    it('should be area for given length and breadth ', function () {
      let rectangle = new Rectangle(3, 8);
      let result = rectangle.area();

      expect(result).toBe(24);
    });
    it('should be 9 for given side of square as 3 ', function () {
      let rectangle = new Rectangle(3);
      let result = rectangle.area();

      expect(result).toBe(9);
    });
    it('should be area for given side of square', function () {
      let rectangle = new Rectangle(5);
      let result = rectangle.area();

      expect(result).toBe(25);
    });
  });
  describe('perimeter', function () {
    it('should be 6 for length 1 and breadth 2', function () {
      let rectangle = new Rectangle(1, 2);
      let result = rectangle.perimeter();

      expect(result).toBe(6);
    });
    it('should be 10 for length 3 and breadth 2', function () {
      let rectangle = new Rectangle(3, 2);
      let result = rectangle.perimeter();

      expect(result).toBe(10);
    });
    it('should be the area for given side of square', function () {
      let rectangle = new Rectangle(5);
      let result = rectangle.perimeter();

      expect(result).toBe(20);
    });
  });

});